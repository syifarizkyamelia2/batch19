console.log("~~~~~~~~~~~~~Soal Nomer 1~~~~~~~~~~~~~\n")

const golden = goldenFunction = () => {
    console.log("This is Golden!!")
}

golden();

console.log("\n\n")





console.log("~~~~~~~~~~~~~Soal Nomer 2~~~~~~~~~~~~~\n")

console.log("object literal")

const newFunction = (firstName, lastName) =>{
    return{
        firstName,lastName,
        fullName(){
            console.log(firstName + " "+ lastName)
            return
        }
    }
}
newFunction("william","imoh").fullName()

console.log("~~~~~~~~~~~~~Soal Nomer 3~~~~~~~~~~~~~\n")

const newObject = {
    firstName: "Harry",
    lastName: "Potter Holt",
    destination: "Hogwarts React Conf",
    occupation: "Deve-wizard Avocado",
    spell: "Vimulus Renderus!!!"
}

const { firstName, lastName, destination, occupation } = newObject;

console.log(newObject)
console.log("\n\n")
console.log("~~~~~~~~~~~~~Soal Nomer 4~~~~~~~~~~~~~\n")

const west = ["Will", "Chris", "Sam", "Holly"]
const east = ["Gill", "Brian", "Noel", "Maggie"]

let combinedArray = [...west, ...east]

console.log(combinedArray)
console.log("\n\n")
console.log("~~~~~~~~~~~~~Soal Nomer 5~~~~~~~~~~~~~\n")

const planet = "earth"
const view = "glass"
const before = 'Lorem ' + view + 'dolor sit amet, ' +
    'consectetur adipiscing elit,' + planet + 'do eiusmod tempor ' +
    'incididunt ut labore et dolore magna aliqua. Ut enim' +
    ' ad minim veniam'

const template = `${planet} ${view} ${before}`

console.log(template)