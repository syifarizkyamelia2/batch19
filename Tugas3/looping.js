console.log("====== NO 1 ======")
var angka = 2;

console.log("LOOPING PERTAMA");

while (angka <= 20) {
    console.log(angka + " I love coding");
    angka += 2;
}

var coba = 20
var dua = 2;
console.log("LOOPING KEDUA");

while (coba >= 2) {
    console.log(coba + " I will become a mobile developer");
    coba -= 2;
}

console.log("====== NO 2 ======")
var santai = " - Santai"
var berkualitas = " - Berkualitas"
var lovecoding = " - I Love Coding"

for (i =1 ; i<=20; i++){
    if(i % 2 != 1){
        console.log(i + berkualitas)
    } else if (i % 3 == 0){
        console.log(i + lovecoding)
    } else {
        console.log(i + santai)
    }
}
console.log("====== NO 3 ======")
var i = 1;
var j = 1;
var panjang = 8;
var lebar = 4;
var pagar = " ";

while(j <= lebar){
    while (i <= panjang){
        pagar += "#";
        i++;
    }
    console.log(pagar);
    pagar=' ';
    i=1;
    j++;
}
console.log("====== NO 4 ======")
i = 1;
j = 7;
var alas = 7;
tinggi = 7 ;
var pagar = "";

for (i=1; i<= tinggi; i++){
    for(j= 1; j<= i; j++){
        pagar +="#";
    }
    console.log(pagar)
    pagar="";
}

console.log("====== NO 5 ======")
i = 1;
j= 1;
var panjang= 8;
var lebar = 8;
var papan ="";

for (j = 1; j<= lebar; j++){
    if(j%2== 1){
        for(i=1; i<=panjang; i++){
            if(i% 2==1){
                papan +=" "
            } else{
                papan +="#"
            }
        }
    }else{
        for (i = 1; i<= panjang; i++){
            if(i%2 ==1){
                papan +="#"
            }else {
                papan +=" "
            }
        }
    }
    console.log(papan);
    papan = "";
}

for(var i = 10; i < 20; i++) {
    console.log('Iterasi ke-' + i);
  } 
  

  var jumlah = 0;
  for(var deret = 6; deret > 0; deret--) {
    jumlah += deret;
    console.log('Jumlah saat ini: ' + jumlah);
  }
   
  console.log('Jumlah terakhir: ' + jumlah);



  for(var deret = 0; deret <= 10; deret += 2) {
    console.log('Iterasi dengan Increment counter 2: ' + deret);
  }
   
  console.log('-------------------------------');
   
  for(var deret = 15; deret >= 0; deret -= 3) {
    console.log('Iterasi dengan Decrement counter : ' + deret);
  } 
  
